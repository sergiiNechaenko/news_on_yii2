<?php
use yii\helpers\Url;

?>

    <input type="text" placeholder="search" style="
                border:2px solid black;
                padding: 5px;
                margin: 5px;" class="search-news" id="search-news">

<?php
// Url::to() calls UrlManager::createUrl() to create a URL
//$url = Url::to(['main/view', 'id' => 100]);

?>
<div class="search-result" id="search-result">
<?php
foreach ($items as $item){
?>
    <a href="<?php echo Url::to(['main/view', 'newsId' => $item->id]);?>">
        <div class="news_item" style="
                border:2px solid black;
                padding: 5px;
                margin: 5px;
            ">
                <h1><?php echo $item->title; ?></h1>
                <img src="<?php echo $item->image; ?>" alt="not_found">
                <h2><?php echo $item->date; ?></h2>
        </div>
    </a>
<?php
}
?>
</div>
<script>
    var search_route = '<?php echo Url::to(['main/find', 'nameForSearch' => '']);?>';
    var search_news_route = '<?php echo Url::to(['main/view', 'newsId' => '']);?>';
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>
    $(document).ready(function () {

        $('#search-news').bind("keyup", function () {

            if(this.value.length >= 0) {

                $.ajax({
                    url: search_route + this.value,
                    success: function (result) {
                        var len = result.length;
                        var str = '';
                        for (var i = 0; i < len; i++) {
                            str += "<a href='"+ search_news_route + result[i].id + "'>";
                            str += "<div class='news_item' style='border:2px solid black;padding: 5px;margin: 5px;'>";
                            str += "<h1>" + result[i].title +"</h1>";
                            str += "<img src='" + result[i].image + "' alt='not_found'>";
                            str += "<h2>" + result[i].date +"</h2>";
                            str += "</div></a>";
                        }
                        $("#search-result").html(str);
                    }
                });

            }

        });
    });

</script>