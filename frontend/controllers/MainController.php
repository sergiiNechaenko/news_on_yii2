<?php

namespace frontend\controllers;

use app\models\News;


class MainController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $items = News::find()->orderBy([
            'date' => SORT_DESC,
        ])->all();

        return $this->render('index',array('items' => $items));
    }

    public function actionView($newsId)
    {

        $item = News::find()->where(['id' => $newsId])->one();

        return $this->render('view',array('item' => $item));
    }

    public function actionFind($nameForSearch)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $items = News::find()
            ->where('news.title LIKE "%'.$nameForSearch.'%"')
            ->orWhere('"'.$nameForSearch.'" LIKE CONCAT("%", news.title, "%")')
            ->orderBy([
                'date' => SORT_DESC,
            ])
            ->all();

        return $items;
    }

}
